# Run the container
docker run -it -v /data/PROJECTS/ASGARD/src/hackathon3-may-mallorca/src/iLID_DOCKER/acoustic_corpus:/opt/IN -v /data/PROJECTS/ASGARD/src/hackathon3-may-mallorca/src/iLID_DOCKER/OUT:/opt/OUT -v /data/PROJECTS/ASGARD/src/hackathon3-may-mallorca/src/iLID_DOCKER/iLID:/opt/iLID ilid

### avoid import features error
vim /opt/iLID/preprocessing/audio/melfilterbank.py

# Replace: 	import features as speechfeatures
# For: 		import python_speech_features as speechfeatures

### Preprocess the audios

## Set the SPARK home
export SPARK_HOME=/opt/spark/spark-2.0.2

##### Create the output folders #####
mkdir -p /opt/OUT/en/train
mkdir -p /opt/OUT/en/test
mkdir -p /opt/OUT/es/train
mkdir -p /opt/OUT/es/test

### Train ###
## EN
preprocessing/run.sh --inputPath /opt/IN/english/train/ --outputPath /opt/OUT/en/train &> /opt/OUT/logs/preprocess_en_train.log &

## ES
preprocessing/run.sh --inputPath /opt/IN/spanish/train/ --outputPath /opt/OUT/es/train &> /opt/OUT/logs/preprocess_es_train.log &


### Test ###

## EN
preprocessing/run.sh --inputPath /opt/IN/english/test/ --outputPath /opt/OUT/en/test &> /opt/OUT/logs/preprocess_en_test.log &

## ES
preprocessing/run.sh --inputPath /opt/IN/spanish/test/ --outputPath /opt/OUT/es/test &> /opt/OUT/logs/preprocess_es_test.log &


##### TRAIN ###############

#~ In models/Berlin_net:
#~ Replace: 	HOME="/home/mpss2015m_1/DeepAudio/iLID/"
#~ for:		HOME="/opt/iLID/" 


