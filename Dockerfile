FROM vicomtech/deep-base-gpu

ENV GPU_SUPPORT=1 \
 PYTHONPATH="/workdir/frameworks/mxnet/src/python:/workdir/frameworks/caffe/src/python:/workdir/frameworks/caffe/src/python:" \
 PATH="/workdir/frameworks/torch/src/install/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/cuda/bin" \
 DYLD_LIBRARY_PATH="/workdir/frameworks/torch/src/install/lib:" \
 LD_LIBRARY_PATH="/workdir/frameworks/torch/src/install/lib::/usr/local/cuda/lib64" \
 LUA_CPATH="/workdir/frameworks/torch/src/install/lib/?.so;/root/.luarocks/lib/lua/5.1/?.so;/workdir/frameworks/torch/src/install/lib/lua/5.1/?.so;./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so" \
 LUA_PATH="/root/.luarocks/share/lua/5.1/?.lua;/root/.luarocks/share/lua/5.1/?/init.lua;/workdir/frameworks/torch/src/install/share/lua/5.1/?.lua;/workdir/frameworks/torch/src/install/share/lua/5.1/?/init.lua;./?.lua;/workdir/frameworks/torch/src/install/share/luajit-2.1.0-beta1/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua"

RUN apt-get update --fix-missing && \
	apt-get -y install \
	curl \
	git-core \
	python \
	python-numpy \
	python-scipy \
	python-dev \
	python-pip \
	vim	\
	wget

WORKDIR /workdir
ADD . /workdir

RUN make global_dependencies 

# OpenCV

WORKDIR /opt

RUN git clone https://github.com/Itseez/opencv.git && \
	git clone https://github.com/Itseez/opencv_contrib.git
	
	
	
#	cd opencv 
#	#&& \ git checkout 3.0.0

#WORKDIR /opt
#RUN git clone https://github.com/Itseez/opencv_contrib.git && \
	#cd opencv_contrib 
	#&& \ git checkout 3.0.0

WORKDIR /opt/opencv
RUN mkdir -p build
WORKDIR /opt/opencv/build
RUN cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
ENV CMAKE_BUILD_TYPE=Release\Debug \
 OPENCV_EXTRA_MODULES_PATH=/opt/opencv_contrib/modules/ \
 BUILD_DOCS=/opt/opencv/build/doc \
 BUILD_EXAMPLES=/opt/opencv/build/examples/ \
 PYTHON2_EXECUTABLE=/usr/bin/python \
 PYTHON_INCLUDE_DIR=/usr/include/python2.7/ \
 PYTHON_INCLUDE_DIR2=/usr/include/x86_64-linux-gnu/python2.7/ \
 PYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython2.7.so \
 PYTHON2_NUMPY_INCLUDE_DIRS=/usr/lib/python2.7/dist-packages/numpy/core/include/ \
 SPARK_HOME=\"/opt/spark/spark-2.0.2/\".
RUN mkdir examples && \
	make -j16

## SPARK

RUN mkdir -p /opt/spark && \
	curl http://d3kbcqa49mib13.cloudfront.net/spark-2.0.2.tgz | tar xvz -C /opt/spark && \
	cd /opt/spark/spark-2.0.2 && \
	./build/mvn -DskipTests clean package
 
RUN echo "export SPARK_HOME=\"/opt/spark/spark-2.0.2/\"" >> ~/.bashrc

# iLid
RUN mkdir -p /opt
WORKDIR /opt
RUN git clone https://github.com/twerkmeister/iLID
WORKDIR /opt/iLID
RUN sed -i 's/import features as speechfeatures/import python_speech_features as speechfeatures/' /opt/iLID/preprocessing/audio/melfilterbank.py && \
	pip install --upgrade pip  && \
	pip install -r requirements.txt && \
	pip install python_speech_features && \
	pip install youtube_dl 

